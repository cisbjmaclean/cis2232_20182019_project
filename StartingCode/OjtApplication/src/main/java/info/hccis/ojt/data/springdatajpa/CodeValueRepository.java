package info.hccis.ojt.data.springdatajpa;

import info.hccis.camper.model.jpa.CodeValue;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodeValueRepository extends CrudRepository<CodeValue, Integer> {
    List<CodeValue> findByCodeTypeId(int codeTypeId);
}