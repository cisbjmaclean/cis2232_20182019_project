﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CamperWebServiceClient.CamperSoapWebService;
using System.Web.Services.Protocols;

namespace CamperWebServiceClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGetName_Click(object sender, EventArgs e)
        {
            CamperSoapWebService.CamperSoap csws = new CamperSoapWebService.CamperSoap();
            int camperId = Convert.ToInt32(txtBxCamperId.Text);

            String name = csws.getCamper(camperId).firstName;

            lblName.Text = name;
        }
    }
}
