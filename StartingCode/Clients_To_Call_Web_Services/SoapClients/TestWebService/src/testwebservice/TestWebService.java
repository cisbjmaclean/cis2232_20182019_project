package testwebservice;

import info.hccis.camper.soap.Camper;
import info.hccis.camper.soap.CamperSoap;
import info.hccis.camper.soap.CamperSoap_Service;

/**
 *
 * @since Nov 8, 2017
 * @author bjmaclean
 */
public class TestWebService {

    public static void main(String[] args) {

CamperSoap_Service camperService = new CamperSoap_Service();
CamperSoap camper = camperService.getCamperSoapPort();
Camper camperFound = camper.getCamper(1);
System.out.println(camperFound.getFirstName());

    }

}
